var Disk = require('sails-disk')

module.exports = {

  adapters: {
    disk: Disk
  },

  connections: {
    contactsDb: {
      adapter: 'disk',
      filePath: process.env.DB_PATH,
      fileName: 'contacts.db'
    }
  },

  defaults: {
    migrate: 'alter',
    schema: true,
    autoPK: false
  }

}
