'use strict'

module.exports = (App, mode) => {

  process.env.DB_PATH   = 'database/'
  process.env.NODE_ENV  = process.argv[2] || mode

  require('./Environment')(App)
  require('../orm')(App)
  require('../api/routes')(App)

}
