
module.exports = {

  getAll: function(req, res, next) {
    req.models.contact.find(req.query, function(err, contacts){
      if (err) return res.json({error: err}, 500);
      res.status(200).json(contacts);
    })
  },

  create: function(req, res, next) {
    req.models.contact.create(req.body, function(err, contact) {
      if (err) return next(err);
      res.status(201).json(contact);
    });
  },

}
