var Waterline = require('waterline')

module.exports = Waterline.Collection.extend({

  identity: 'contact',

  connection: 'contactsDb',

  attributes: {
    id: {
      type: 'integer',
      autoIncrement: true,
      primaryKey: true,
      unique: true
    },
    first_name: {
      type: 'string'
    },
    last_name: {
      type: 'string'
    },
    birth_date: {
      type: 'string'
    },
    phone: {
      type: 'string'
    },
    email: {
      type: 'string'
    },
    note: {
      type: 'string'
    }
  }

})
