'use strict'

var Router = require('express').Router()
  , Controllers = {
      Contacts: require('../controllers/Contacts')
    }

Router.get('/', Controllers.Contacts.getAll)
      .post('/', Controllers.Contacts.create)

module.exports = Router
