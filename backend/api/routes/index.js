'use strict'

var bodyParser  = require('body-parser')
  , Routes      = {
      contact: require('./Contact')
    }

module.exports = (App) => {

  App
    .use(function(req, res, next) {
      req.models = App.models
      next()
    })

    .use(bodyParser.urlencoded({extended:true}))
    .use(bodyParser.json())

    .use('/api/contacts', Routes.contact)

    .get('/', function(req,res){
      res.render('index');
    })

}
