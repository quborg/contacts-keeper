'use strict'

var Waterline   = require('waterline')
  , Orm         = new Waterline()
  , Connection  = require('../configs/Connection.js')
  , Contact        = require('../api/models/Contact.js')

Orm.loadCollection(Contact)

module.exports = (App) => {
  Orm.initialize(Connection, function(err, models) {
    if(err) throw err
    App.models = models.collections
    App.connections = models.connections
  })
}
