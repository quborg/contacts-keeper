'use strict'

import React            from 'react';
import Assign           from 'object-assign';
import Table            from '../templates/Table';
import Search           from '../templates/Search';
import Modal            from '../templates/Modal';
import Helper           from '../../utils/Helper';
import Store            from '../../stores/Store';
import Actions          from '../../actions';
import connectToStores  from 'alt-utils/lib/connectToStores';
import { Row, Col, Button, Glyphicon } from 'react-bootstrap';


@connectToStores
export default class ContactComponent extends React.Component {

  constructor (props, context) {
    super(props, context);
    this.state = {
      open: false,
      filter: '',
      structure: context.structure,
      contacts: []
    };
  }

  static contextTypes = {
    structure: React.PropTypes.object
  }

  static getStores () {
    return [Store]
  }

  static getPropsFromStores () {
    return Store.getState()
  }

  componentDidMount() {
    Actions.getContacts((contacts) => {this.setState({contacts: contacts}) })
  }

  hide = () => {
    this.setState({open: false})
  }

  open = () => {
    this.setState({open: true})
  }

  search = e => {
    this.setState({filter: e.target.value });
    let results = Helper.filter(e.target.value, this.props.contacts);
    console.log(results);
    this.setState({ contacts: results })
  }

  updateContacts = (contact) => {
    let contacts = Assign([], this.state.contacts)
    contacts.push(contact)
    this.setState({ contacts: contacts })
  }

  save = (contact) => {
    Actions.addContact(contact, this.updateContacts)
    this.hide()
  }

  render () {
    // console.log(this.props.contacts,this.state.contacts)
    return (
      <Row>
        <div className="contacts-tool">
          <Col xs={3}>
            <Search value={this.state.filter} search={this.search}/>
          </Col>

          <Col className="text-right" xs={3} xsOffset={6}>
            <Button bsStyle="primary" className="add-contact" onClick={this.open}>
              <Glyphicon glyph="plus-sign"/>
              Contacts Keeper
            </Button>
          </Col>
        </div>

        <Col xs={12}>
          <Table
            columns={this.state.structure.Views.Contacts.thead}
            data={this.state.contacts}/>
        </Col>

        <Modal
          show={this.state.open}
          hide={this.hide}
          onHide={this.hide}
          save={this.save}/>
      </Row>
    )
  }

}
