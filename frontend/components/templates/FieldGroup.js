'use strict';

import React from 'react';
import { FormGroup, ControlLabel, FormControl } from 'react-bootstrap';


export default class FieldGroupComponent extends React.Component {

  static defaultProps = {
    type: '',
    name: '',
    value: '',
    label: ''
  }

  render () {
    let {label, type, name, value, handler} = this.props;
    return (
      <FormGroup>
        <ControlLabel>{label}</ControlLabel>
        { 
          type == 'textarea'
          ? <FormControl value={value} onChange={handler} name={name} componentClass={type}/>
          : <FormControl value={value} onChange={handler} name={name} type={type}/>
        }
      </FormGroup>
    )
  }

}
