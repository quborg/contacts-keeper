'use strict'

import React      from 'react';
import ReactDOM   from 'react-dom';
import FieldGroup from './FieldGroup';
import { Row, Col, Modal, Button, Glyphicon } from 'react-bootstrap'


export default class ModalComponent extends React.Component {

  constructor () {
    super();
    this.state = {
      first_name: '',
      last_name: '',
      birth_date: '',
      phone: '',
      email: '',
      note: ''
    };
  }

  handleValue = e => {
    switch (e.target.name) {
      case 'first_name': this.setState({ first_name: e.target.value }); break;
      case 'last_name':  this.setState({ last_name:  e.target.value }); break;
      case 'birth_date': this.setState({ birth_date: e.target.value }); break;
      case 'phone':      this.setState({ phone:      e.target.value }); break;
      case 'email':      this.setState({ email:      e.target.value }); break;
      case 'note':       this.setState({ note:       e.target.value }); break;
    }
  }

  save = () => {
    this.setState({
      first_name: '',
      last_name: '',
      birth_date: '',
      phone: '',
      email: '',
      note: ''
    })
    this.props.save(this.state)
  }

  render () {
    let {show, onHide, hide, save} = this.props
    return (
      <Modal show={show} onHide={onHide}>
        <Modal.Header>
          <Modal.Title>
            <span>Contacts Keeper</span>
            <Glyphicon className="pointer right" glyph="remove-sign" onClick={hide}/>
          </Modal.Title>
        </Modal.Header>
        
        <Modal.Body>
          <Row>
            <Col xs={5}>
              <FieldGroup
                type="text"
                name="first_name"
                label="First Name"
                value={this.state.first_name}
                handler={this.handleValue}/>
            </Col>
            <Col xs={5} xsOffset={2}>
              <FieldGroup
                type="text"
                name="last_name"
                label="Last Name"
                value={this.state.last_name}
                handler={this.handleValue}/>
            </Col>
          </Row>

          <Row>
            <Col xs={5}>
              <FieldGroup
                type="text"
                name="birth_date"
                label="Date of Birth"
                value={this.state.birth_date}
                handler={this.handleValue}/>
            </Col>
            <Col xs={5} xsOffset={2}>
              <FieldGroup
                type="text"
                name="phone"
                label="Phone Number"
                value={this.state.phone}
                handler={this.handleValue}/>
            </Col>
          </Row>

          <Row>
            <Col xs={5}>
              <FieldGroup
                type="text"
                name="email"
                label="Email"
                value={this.state.email}
                handler={this.handleValue}/>
            </Col>
          </Row>

          <Row>
            <Col xs={12}>
              <FieldGroup
                type="textarea"
                name="note"
                label="Notes"
                value={this.state.note}
                handler={this.handleValue}/>
            </Col>
          </Row>
        </Modal.Body>

        <Modal.Footer>
          <Button className="save-contact" onClick={this.save}>Save</Button>
        </Modal.Footer>
      </Modal>
    )
  }

}
