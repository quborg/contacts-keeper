'use strict'

import React     from 'react';
import Structure from '../../settings';
import { Table } from 'react-bootstrap';


export default class TableComponent extends React.Component {

  render () {
    let T = this.props
      , Model = Structure.Views.Contacts.model;
    console.log(T)
    return (
      <Table condensed hover responsive className="contacts-table">
        <thead>
          <tr>
            {
              Model.map( (column, i) => {
                return <th key={i} width={column.width}>{column.title}</th>
              })
            }
          </tr>
        </thead>
        <tbody>
          {
            T.data.length ?
              T.data.map( (contact, i) => {
                return (
                  <tr key={i} id={contact.id}>
                    { 
                      Model.map( (attr, i) => {
                        return <td key={i}>{contact[attr.name]}</td>
                      })
                    }
                  </tr>
                )
              })
            : [1,2,3,4,5,6,7].map((v,k) => { return (<tr key={k}>{[1,2,3,4,5,6].map( (m,n) => { return <td key={n} className="fake-cell"></td> }) }</tr> )})
          }
        </tbody>
      </Table>
    )
  }

}
