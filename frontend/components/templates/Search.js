'use strict'

import React from 'react';
import { FormGroup, InputGroup, FormControl, Button, Glyphicon } from 'react-bootstrap';


export default class SearchComponent extends React.Component {

  render () {
    let S = this.props;
    return (
      <form>
        <FormGroup>
          <InputGroup>

            <FormControl
              className="search-input"
              type="text"
              value={S.value}
              onChange={S.search}
              placeholder="Search"/>

            <InputGroup.Button>
              <Button bsStyle="primary" className="default-cursor" disabled>
                <Glyphicon glyph="search"/>
              </Button>
            </InputGroup.Button>

          </InputGroup>
        </FormGroup>
      </form>
    )
  }

}
