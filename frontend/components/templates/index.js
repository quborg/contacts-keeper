'use strict';

import React            from 'react';
import { Grid }         from 'react-bootstrap';
import Menu             from './Menu';
import Structure        from '../../settings';
import ContextProvider  from '../../utils/ContextProvider';

let Context     = { structure: Structure }
  , ContextType = { structure: React.PropTypes.object }
  ;


@ContextProvider(Context, ContextType)
export default class TemplateComponent extends React.Component {

  render() {
    return (
      <Grid fluid>
        <Menu/>
        <div className="container">
          {this.props.children}
        </div>
      </Grid>
    )
  }

}
