'use strict';

import _ from 'lodash';


let Helper = {

  /*
  ** @
  */
  filter(keyword, data) {
    let results = []
      , toOmit = ['createdAt', 'updatedAt', 'id']
      ;
    
    _.map(data, (item) => {
      let cleanItem = _.omit(item, toOmit)
        , flow = ''
        ;
      _.map(cleanItem, (v) => {flow += v})
      flow.indexOf(keyword) !== -1
      ? results.push(item)
      : null
    })
    
    return results;
  }

}

export default Helper;
