const raw = 'http://localhost:3000' ;
const base = raw + '/api/';

module.exports = {

    get: { contacts: base + 'contacts' },
    post: { contact: base + 'contacts' }

};
