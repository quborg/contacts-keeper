// import alt      from '../alt';
import Api      from '../utils/Api';
import Actions  from '../actions';
import Request  from 'superagent';


let DataSources = {

  loadContacts: {
    remote (state) {
      return new Promise ( (resolve, reject) => {
        Request
          .get(Api.get.contacts)
          .end(function (err, res) {
            if (err) {
              reject(false)
            } else {
              resolve(res.body);
            }
          });        
      })
    },
    success: Actions.contactsReceived,
    error: Actions.contactsFailed
  }

}

export default DataSources;
