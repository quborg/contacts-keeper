'use strict';

import React      from 'react';
import Template   from '../components/templates';
import Contacts   from '../components/contacts';

import { Router, Route, IndexRoute, browserHistory } from 'react-router';

module.exports = (
  <Router history={browserHistory}>
    <Route path="/" component={Template}>
      <IndexRoute component={Contacts}/>
    </Route>
  </Router>
)
