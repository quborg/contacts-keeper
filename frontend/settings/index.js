'use strict'

module.exports = {

  app_name: "Contacts Keeper",

  Views: {
    Contacts: {
      model: [
        { name: "first_name",     title: "First Name",     width:"16%"},
        { name: "last_name",      title: "Last Name",      width:"16%"},
        { name: "birth_date",     title: "Date of Birth",  width:"13%"},
        { name: "phone",          title: "Phone",          width:"14%"},
        { name: "email",          title: "Email",          width:"16%"},
        { name: "note",           title: "Notes",          width:"auto"}
      ],
      addModal: {
        title: "Contacts Keeper"
      }
    }
  }

}
