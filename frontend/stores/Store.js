import alt from '../alt';
import Actions from '../actions';
import {decorate, bind, datasource} from 'alt-utils/lib/decorators';


@decorate(alt)
class ContactsStore {
  constructor () {
    this.state = {
      contacts: [],
      state: ''
    };
  }

  @bind(Actions.getContacts)
  getContacts (contacts) {
    this.setState({contacts: contacts});
  }

  @bind(Actions.addContact)
  addContact (state) {
    this.setState({ state: state });
  }

}

export default alt.createStore(ContactsStore);
