'use strict';

import React    from 'react';
import ReactDOM from 'react-dom';
import Routes   from './settings/Routes';


ReactDOM.render(Routes, document.getElementById('main'));
