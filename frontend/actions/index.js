import alt      from '../alt';
import Api      from '../utils/Api';
import Request  from 'superagent';


class Actions {

  getContacts (callback) {
    return (dispatch) => {
      Request
        .get(Api.get.contacts)
        .end(function (err, res) {
          if (err) {
            dispatch(false)
          } else {
            dispatch(res.body);
            if (callback) callback(res.body);
          }
        });
    }
  }

  addContact (contact, callback) {
    return (dispatch) => {
      Request
        .post(Api.post.contact)
        .send(contact)
        .end(function (err, res) {
          if (err) {
            dispatch(false)
          } else {
            dispatch(res.body);
            if (callback) callback(res.body);
          }
        });
    }
  }

}

module.exports = alt.createActions(Actions);
