var Alt = require('alt');
var alt = new Alt(); // this instantiates a Flux dispatcher for you and gives you methods to create your actions and stores.

module.exports = alt;
