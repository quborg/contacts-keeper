'use strict';

let gulp        = require('gulp')
  , gutil       = require('gulp-util')
  , browserSync = require('browser-sync')
  , browserify  = require('browserify')
  , source      = require('vinyl-source-stream')
;

gulp
  .task('browserify', function() {
    gutil.log('Compiling JS....')
    browserify('src/main.js')
      .transform('babelify', { 
        presets: ["es2015","stage-1","stage-0","react"],
        plugins: ['transform-runtime','transform-decorators-legacy']
      })
      .bundle()
      .on('error', function (err) {
          gutil.log(err.message)
          browserSync.notify("Browserify Error! :", err)
          this.emit("end")
        })
      .pipe(source('main.js'))
      .pipe(gulp.dest('public/js'))
      .pipe(browserSync.stream({once: true})
    )
  })

  .task('sync', function() {
    browserSync.init({
      server: 'public'
     // ,files: ['public']
     ,port: 8000
    })
  })

  .task('default', ['browserify', 'sync'], function() {
    gulp.watch('frontend/**/*.*', ['browserify'], browserSync.reload() )
    gulp.watch('public/styles/**/*.*', function() { browserSync.reload() } )
  })
;
